use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{json, Value};
use log;

// Define the async function to show input data
async fn show_input_data(event: Value, _: Context) -> Result<Value, Error> {
    // Construct response JSON echoing the input data
    let response = json!({
        "status": "Input Data Echoed",
        "input_data": event
    });

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Create a handler function using `handler_fn` macro
    let show_input_data_func = handler_fn(show_input_data);
    
    // Run the Lambda function
    match lambda_runtime::run(show_input_data_func).await {
        Ok(_) => log::info!("Input data showing function executed successfully"),
        Err(err) => log::error!("Error showing input data: {}", err),
    }
    
    Ok(())
}
