use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{json, Value};
use log;

// Define the async function to process data
async fn process_data(event: Value, _: Context) -> Result<Value, Error> {
    // Extract numbers array from the input event
    let numbers = event["input_data"]["numbers"].as_array().ok_or("Numbers array not found")?;
    
    // Calculate the sum of numbers
    let sum: i64 = numbers.iter()
        .filter_map(|num| num.as_i64())
        .sum();
    
    // Construct response JSON
    let response = json!({
        "status": "Processing Complete",
        "result": {
            "sum": sum
        }
    });

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Create a handler function using `handler_fn` macro
    let process_data_func = handler_fn(process_data);
    
    // Run the Lambda function
    match lambda_runtime::run(process_data_func).await {
        Ok(_) => log::info!("Data processing function executed successfully"),
        Err(err) => log::error!("Error processing data: {}", err),
    }
    
    Ok(())
}
