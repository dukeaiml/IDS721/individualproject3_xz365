# Individual Project4: Rust AWS Lambda and Step Functions
> Xingyu, Zhang (NetID: xz365)

## Setting Up The Rust Functions
Simple guidance for creating, building and deploying a Rust AWS Lambda function:
```bash
# create
cargo new func_name

# move into the func dir and omplete the function in the func_name folder
cd func_name

# build the function
cargo lambda build --release

# deploy the function to the AWS Lambda (add '--iam-role' if needed)
cargo lambda deploy
```



Basically, there are two functions included in this whole project:
1. `IP4-input`: This function simply echoes back the input data in the response. [Dependencies](./IP4-input/Cargo.toml)| [Rust Code](./IP4-input/src/main.rs)
![input-func](./assets/input-func.png)
    - The input of this function should be in format of {"numbers": `ARRAY`}, for example
    ```json
    {
        "numbers": [
            1,
            2,
            3
        ]
    }
    ```
    - The output of this function depends on whether the input is valid. If the answer is yes, the response of the previous example will be
    ```json
    {
        "input_data": {
            "numbers": [
                1,
                2,
                3
                ]
        },
        "status": "Input Data Echoed"
    }
    ```
    - Test result:
![input-test](./assets/input-test.png)


2. `IP4-sum`: This function perform a data processing task of calculating the sum of numbers provided in the input event.[Dependencies](./IP4-sum/Cargo.toml)| [Rust Code](./IP4-sum/src/main.rs)
![sum-func](./assets/sum-func.png)
    - The input of this function should be align with the output of the previous function, to be more specific, in previous example case
    ```json
    {
        "input_data": {
            "numbers": [
                1,
                2,
                3
                ]
        },
        "status": "Input Data Echoed"
    }
    ```
    - The output of this function depends on the process status. If the data is processed sucessfully, the response of the this case will be
    ```json
    {
        "result": {
            "sum": 6
        },
        "status": "Processing Complete"
    }
    ```
    - Test result:
![sum-test](./assets/sum-test.png)

## Building Step Functions
First, we need to create a new state machine in the [Step Functions](https://us-east-1.console.aws.amazon.com/states/home?region=us-east-1#/homepage) section in AWS Console. Consider the working flow of data processing is show the input first and then calculate the sum of the input, the designed state machine has a structure:
![state machine](./assets/state_machine.png) 

Then executution result is:
![show input result](./assets/input-state.png)
![sum result](./assets/sum-state.png)

- Demo Video that performing the whole process is in the root directory and named `demo.mkv`.
